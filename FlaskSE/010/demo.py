from flask import Flask, request
import os

app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World !'

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)

from flask import Flask
app = Flask(__name__)


@app.route('/api/<name>')
def apiPathString(name):
    print("type(name) : ", type(name))
    return 'String => {}'.format(name)


@app.route('/api/<int:row_id>')
def apiPathInteger(row_id):
    print("type(id) : ", type(row_id))
    return 'int => {}'.format(row_id)


@app.route('/api/<float:version>')
def apiPathFloat(version):
    print("type(version) : ", type(version))
    return 'float => {}'.format(version)


if __name__ == '__main__':
    app.run()

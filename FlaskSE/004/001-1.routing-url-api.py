from flask import Flask
app = Flask(__name__)


@app.route('/')
@app.route('/home')
def home():
    home = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
    </head>
    <body>
        <h1>Hello World</h1>
        <p> This is my website</p>
    </body>
    </html>
    """
    return home


@app.route('/message/hello')
def hello():
    hello = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Hello</title>
    </head>
    <body>
        Hello World
    </body>
    </html>
    """
    return hello


if __name__ == '__main__':
    app.run()

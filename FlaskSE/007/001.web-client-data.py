from flask import Flask, render_template, request, jsonify, json, make_response
app = Flask(__name__)


lstData = [
    'PythonSE',  'PythonDoc',  'FlaskSE',  'PythonSQL',  'PythonOCR',
    'Html',  'jQuery',  'CSS-Sty',  'Java',  ' SQL-SE'
]

@app.route('/')
@app.route('/data/js')
def js_cookie_page():
    return render_template('cookie/001.javascript-data.html')


@app.route('/data/jquery')
def jquery_cookie_page():
    return render_template('cookie/002.jquery-data.html')


@app.route('/data/flask')
def flask_cookie_page():
    return render_template('cookie/003-1.flask-data.html')


@app.route('/login', methods=['POST'])
def flask_login():
    if request.method == 'POST':
        account = request.form['account']
        password = request.form['password']       

        isRememberMe = False
        if 'isRememberMe' in request.form :
            isRememberMe = request.form['isRememberMe'] == 'true'

        print("login => ")
        print("account : " , account)
        print("password : " , password)
        print("isRememberMe : " , isRememberMe)

        response = make_response(render_template('cookie/003-2.flask-login.html' , lstData = lstData))

        if isRememberMe :
            response.set_cookie('account', account)
            response.set_cookie('isRememberMe' , "true")
        return response


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)

from flask import Flask, render_template, request, jsonify, json
app = Flask(__name__)

@app.route('/')
@app.route('/web')
def page():
    return render_template('005.web-api.html')

@app.route('/ajax/read', methods=['GET'])
def output_data():
    if request.method == "GET":
        with open('static/data/data.json', 'r') as f:
            data = json.load(f)
            print("data : ", data)
        f.close
        return jsonify(data)  # 直接回傳 data 也可以，都是 json 格式


@app.route('/ajax/write', methods=['POST'])
def input_data():
    if request.method == "POST":
        data = {
            'appInfo': {
                'id': request.form['app_id'],
                'name': request.form['app_name'],
                'version': request.form['app_version'],
                'author': request.form['app_author'],
                'remark': request.form['app_remark']
            }
        }
        print(type(data))
        with open('static/data/data.json', 'w') as f:
            json.dump(data, f)
        f.close
        return jsonify(result='OK')


@app.route('/system/app', methods=['GET', 'POST'])
def system_app():
    if request.method == "GET":
        with open('static/data/database.json', 'r') as f:
            data = json.load(f)
            print("data : ", data)
        return jsonify(data)  # 直接回傳 data 也可以，都是 json 格式
    if request.method == "POST":
        appInfo = {
            'id': int(request.form['app_id']),
            'name': request.form['app_name'],
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        data = None
        with open('static/data/database.json', 'r') as f:
            data = json.load(f)
            data.append(appInfo)
            print("data : ", data)
        with open('static/data/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result='OK')


@app.route('/system/app/<int:app_id>', methods=['GET', 'PUT', 'PATCH', 'DELETE'])
def system_app_id(app_id):
    if request.method == "GET":
        data = None
        with open('static/data/database.json', 'r') as f:
            data = json.load(f)
        appInfo = None
        for d in data:
            if d["id"] == app_id:
                appInfo = d
        return jsonify(appInfo)
    if request.method == "PUT":
        appInfo = {
            'id': int(request.form['app_id']),
            'name': request.form['app_name'],
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        data = None
        with open('static/data/database.json', 'r') as f:
            data = json.load(f)
        for d in data:
            print("d => ", d)
            if d["id"] == app_id:
                d["id"] = appInfo["id"]
                d["name"] = appInfo["name"]
                d["version"] = appInfo["version"]
                d["author"] = appInfo["author"]
                d["remark"] = appInfo["remark"]
        with open('static/data/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result="OK")
    if request.method == "PATCH":
        appInfo = {
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        data = None
        with open('static/data/database.json', 'r') as f:
            data = json.load(f)
        for d in data:
            print("d => ", d)
            if d["id"] == app_id:
                d["version"] = appInfo["version"]
                d["author"] = appInfo["author"]
                d["remark"] = appInfo["remark"]
        with open('static/data/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result="OK")
    if request.method == "DELETE":
        data = None
        with open('static/data/database.json', 'r') as f:
            data = json.load(f)
        index = 0
        for d in data:
            print("d => ", d)
            if d["id"] == app_id:
                break
            index += 1
        data.pop(index)
        with open('static/data/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result="OK")

@app.route('/reset', methods=['POST'])
def reset_data():
    if request.method == "POST":
        text = ""
        with open('static/data/origin.json', 'r') as f:
            text = f.read()
        with open('static/data/data.json', 'w') as f:
            f.write(text)

        text = ""
        with open('static/data/app_info.json', 'r') as f:
            text = f.read()
        with open('static/data/database.json', 'w') as f:
            f.write(text)
        return jsonify(result='OK')

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
from flask import Flask, render_template, request, jsonify, json
app = Flask(__name__)


data = { # dict
    'appInfo': { # dict
        'id': 1,
        'name': 'FlaskSE',
        'version': '1.0.1',
        'author': 'DevAuth',
        'remark': 'Python Flask - Sample Example'
    }
} # jsonify : dict -> json 

@app.route('/')
@app.route('/ajax')
def ajax():
    return render_template('003.web-ajax.html')

@app.route('/ajax/read', methods=['GET'])
def output_data():
    if request.method == "GET":
        return jsonify(data)  # jsonify 轉換 dict 型態為 json 格式

@app.route('/ajax/write', methods=['POST'])
def input_data():
    if request.method == "POST":
        global data
        data = {
            'appInfo': {
                'id': request.form['app_id'],
                'name': request.form['app_name'],
                'version': request.form['app_version'],
                'author': request.form['app_author'],
                'remark': request.form['app_remark']
            }
        }
        print(type(data))
        return jsonify(result='OK')

@app.route('/ajax/reset', methods=['POST'])
def reset_data():
    if request.method == "POST":
        global data
        data = {
            'appInfo': {
                'id': 1,
                'name': 'FlaskSE',
                'version': '1.0.1',
                'author': 'DevAuth',
                'remark': 'Python Flask - Sample Example'
            }
        }
        return jsonify(result='OK')

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
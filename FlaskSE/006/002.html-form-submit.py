from flask import Flask, request, render_template, redirect, url_for
app = Flask(__name__)


@app.route('/')
@app.route('/home')
def formPage():
    return render_template('002.web-form.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        account = request.args.get('account')
        print("get : account => ", account)
        return redirect(url_for('success', user=account, action="get"))
    elif request.method == 'POST':
        account = request.form['account']
        print("post : account => ", account)
        return redirect(url_for('success', user=account, action="post"))


@app.route('/success/<action>/<user>')
def success(action, user):
    return 'Http Method : {} <br> User : {}'.format(action, user)


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)

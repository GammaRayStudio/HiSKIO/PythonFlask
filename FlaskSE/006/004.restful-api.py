from flask import Flask, render_template, request, jsonify, json
app = Flask(__name__)


lstData = [ # list
    { # dict
        "id": 1,
        "name": "PythonSE",
        "version": "1.0.1",
        "author": "DevAuth",
        "remark": "PythonSE-v1.0.1"
    },
    { 
        "id": 2,
        "name": "PythonGUI",
        "version": "1.0.2",
        "author": "DevAuth",
        "remark": "PythonGUI-v1.0.2"
    },
    { 
        "id": 3,
        "name": "FlaskSE",
        "version": "1.0.3",
        "author": "DevAuth",
        "remark": "FlaskSE-v1.0.3"
    }
] # jsonify : list -> json 


@app.route('/')
@app.route('/system')
def page():
    return render_template('004.web-restful.html')

@app.route('/system/app', methods=['GET', 'POST'])
def system_app():
    global lstData
    if request.method == "GET":
        return jsonify(lstData)
    if request.method == "POST":
        appInfo = {
            'id': int(request.form['app_id']),
            'name': request.form['app_name'],
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        lstData.append(appInfo)
        print("data : ", lstData)
        return jsonify(result='OK')


@app.route('/system/app/<int:app_id>', methods=['GET', 'PUT', 'PATCH', 'DELETE'])
def system_app_id(app_id):
    global lstData
    if request.method == "GET":
        for d in lstData:
            if d["id"] == app_id:
                appInfo = d
        return jsonify(appInfo)
    if request.method == "PUT":
        appInfo = {
            'id': int(request.form['app_id']),
            'name': request.form['app_name'],
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        for d in lstData:
            print("d => ", d)
            if d["id"] == app_id:
                d["id"] = appInfo["id"]
                d["name"] = appInfo["name"]
                d["version"] = appInfo["version"]
                d["author"] = appInfo["author"]
                d["remark"] = appInfo["remark"]
        return jsonify(result="OK")
    if request.method == "PATCH":
        appInfo = {
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        for d in lstData:
            print("d => ", d)
            if d["id"] == app_id:
                d["version"] = appInfo["version"]
                d["author"] = appInfo["author"]
                d["remark"] = appInfo["remark"]
        return jsonify(result="OK")
    if request.method == "DELETE":
        index = 0
        for d in lstData:
            print("d => ", d)
            if d["id"] == app_id:
                break
            index += 1
        lstData.pop(index)
        return jsonify(result="OK")

@app.route('/system/reset', methods=['POST'])
def reset_data():
    if request.method == "POST":
        global lstData
        lstData = [ # list
            { # dict
                "id": 1,
                "name": "PythonSE",
                "version": "1.0.1",
                "author": "DevAuth",
                "remark": "PythonSE-v1.0.1"
            },
            { 
                "id": 2,
                "name": "PythonGUI",
                "version": "1.0.2",
                "author": "DevAuth",
                "remark": "PythonGUI-v1.0.2"
            },
            { 
                "id": 3,
                "name": "FlaskSE",
                "version": "1.0.3",
                "author": "DevAuth",
                "remark": "FlaskSE-v1.0.3"
            }
        ] # jsonify : list -> json 
        return jsonify(result='OK')

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
from flask import Flask, render_template
app = Flask(__name__)


@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World'


@app.route('/html')
def html():
    html = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Html</title>
    </head>
    <body>
        <h1>Hello World</h1>
        <p> This is my website</p>
    </body>
    </html>
    """
    return html


@app.route('/page')
def page():
    return render_template('001.web-page.html')


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)

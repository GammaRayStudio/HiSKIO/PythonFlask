from flask import Flask, render_template
app = Flask(__name__)


@app.route('/')
@app.route('/template/page')
def tamplet_page():
    return render_template('003.web-param.html')


@app.route('/template/page/text')
def template_page_text():
    return render_template('003.web-param.html', text="Text Text Text ~ !!!")


@app.route('/template/page/list')
def template_page_dict():
    lstData = [  # list
        '01 - Text Text Text',
        '02 - Text Text Text',
        '03 - Text Text Text',
        '04 - Text Text Text',
        '05 - Text Text Text'
    ]
    return render_template('003.web-param.html', data=lstData)


@app.route('/template/page/data')
def pageAppInfo():
    dataInfo = {  # dict
        'id': 5,
        'name': 'Python - Flask',
        'version': '1.0.1',
        'author': 'Enoxs',
        'remark': 'Python - Web Framework'
    }
    return render_template('003.web-param.html', dataInfo=dataInfo)


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)

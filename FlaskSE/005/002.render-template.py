from flask import Flask, render_template
app = Flask(__name__)


@app.route('/')
@app.route('/web')
def web():
    return render_template('002.web-page.html')


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)

from flask import Flask, render_template, request, jsonify, json
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://DevAuth:Dev127336@127.0.0.1:3306/DevDb"

db = SQLAlchemy(app)
class AppInfo(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column(db.String)
    version = db.Column(db.String)
    author = db.Column(db.String)
    date = db.Column(db.Integer)
    remark = db.Column(db.String)
    
    def __init__(self, name, version, author, date, remark):
        self.name = name
        self.version = version
        self.author = author
        self.date = date
        self.remark = remark

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

@app.route('/')
@app.route('/web')
def page():
    return render_template('web-api.html')

@app.route('/system/app', methods=['GET', 'POST'])
def system_app():
    if request.method == "GET":
        records = AppInfo.query.all()
        data = []
        for appInfo in records :
            data.append(appInfo.as_dict())
        return jsonify(data)  # 直接回傳 data 也可以，都是 json 格式
    if request.method == "POST":
        appInfo = AppInfo(
            request.form['app_name'] , request.form['app_version'] ,
            request.form['app_author'] , datetime.now() , request.form['app_remark']
        )
        db.session.add(appInfo)
        db.session.commit()
        return jsonify(result='OK')

@app.route('/system/app/<int:app_id>', methods=['GET', 'PUT', 'PATCH', 'DELETE'])
def system_app_id(app_id):
    if request.method == "GET":
        appInfo = db.session.query(AppInfo).filter_by(id=app_id).first()
        return jsonify(appInfo.as_dict())
    if request.method == "PUT":
        appInfo = db.session.query(AppInfo).filter_by(id=app_id).first()
        appInfo.name = request.form['app_name']
        appInfo.version = request.form['app_version']
        appInfo.author = request.form['app_author']
        appInfo.date = datetime.now()
        appInfo.remark = request.form['app_remark']
        db.session.commit()
        return jsonify(result="OK")
    if request.method == "PATCH":
        appInfo = db.session.query(AppInfo).filter_by(id=app_id).first()
        appInfo.version = request.form['app_version']
        appInfo.author = request.form['app_author']
        appInfo.remark = request.form['app_remark']
        db.session.commit()
        return jsonify(result="OK")
    if request.method == "DELETE":
        appInfo = db.session.query(AppInfo).filter_by(id=app_id).first()
        db.session.delete(appInfo)
        db.session.commit()
        return jsonify(result="OK")

@app.route('/reset', methods=['POST'])
def reset_data():
    if request.method == "POST":
        db.engine.execute("truncate table app_info")
        data01 = AppInfo('JavaProjSE-v1.0.3','1.0.3','Enoxs','2019-07-24' ,'Java Project Simple Example - Version 1.0.3')
        db.session.add(data01)
        data02 = AppInfo('JUnitSE','1.0.2','Enoxs','2019-08-17' ,'Java Unit Test Simple Example')
        db.session.add(data02)
        data03 = AppInfo('SpringMVC-SE','1.0.2','Enoxs','2019-07-31' ,'Java Web Application Spring MVC')
        db.session.add(data03)
        db.session.commit()
        return jsonify(result='OK')

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
from flask import Flask, request, url_for, redirect, render_template , json , jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import sqlite3

app = Flask(__name__)

# app.config['SQLALCHEMY_DATABASE_URI'] = [DB_TYPE]+[DB_CONNECTOR]://[USERNAME]:[PASSWORD]@[HOST]:[PORT]/[DB_NAME]

# SQLite
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///DevDb.db'

# MySQL
# app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://user_name:password@IP:3306/db_name"
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://DevAuth:Dev127336@127.0.0.1:3306/DevDb"

db = SQLAlchemy(app)
class AppInfo(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column(db.String)
    version = db.Column(db.String)
    author = db.Column(db.String)
    date = db.Column(db.Integer)
    remark = db.Column(db.String)
    
    def __init__(self, name, version, author, date, remark):
        self.name = name
        self.version = version
        self.author = author
        self.date = date
        self.remark = remark

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

@app.route('/')
@app.route('/show')
def show():
    return render_template('show.html', appInfo=AppInfo.query.all())

@app.route('/query/sql')
def queryBySQL():
    conn = sqlite3.connect('DevDb.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM `app_info`;')
    records = cursor.fetchall()
    return jsonify(records)

@app.route('/query/orm')
def queryByORM():
    records = AppInfo.query.all()
    result = []
    # demo
    print(records[0].as_dict())
    for appInfo in records:
        result.append(appInfo.as_dict())
    return jsonify(result)

@app.route('/add', methods=['POST'])
def add():
    if request.method == 'POST':
        info = AppInfo(request.form['name'], request.form['version'],
                        request.form['author'], request.form['date'], request.form['remark'])
        db.session.add(info)
        db.session.commit()
        return redirect(url_for('show'))

@app.route('/delete/<app_id>', methods=['POST'])
def delete(app_id):
    if request.method == 'POST': 
        appInfo = db.session.query(AppInfo).filter_by(id=app_id).first()
        print(appInfo)
        db.session.delete(appInfo)
        db.session.commit()
    return redirect(url_for('show'))

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)

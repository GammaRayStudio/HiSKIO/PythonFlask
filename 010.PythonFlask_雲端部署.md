Python Flask : 雲端部署
======


雲端部署 Heroku 並且連線 MySQL
------
### 什麼是 Heroku ?

**維基百科 : Heroku**

    Heroku是一個支援多種程式語言的雲平台即服務。
    在2010年被Salesforce.com收購。Heroku作為最元祖的雲平台之一，從2007年6月起開發，當時它僅支援Ruby，
    但後來增加了對Java、Node.js、Scala、Clojure、Python以及（未記錄在正式檔案上）PHP和Perl的支援。
    基礎作業系統是Debian，在最新的技術堆疊則是基於Debian的Ubuntu。

+ <https://zh.wikipedia.org/wiki/Heroku>

**維基百科 : 平台即服務**

    平台即服務（英語：platform as a service，縮寫：PaaS）是一種雲端運算服務，提供運算平台與解決方案服務。
    使用者不需要管理與控制雲端基礎設施（包含網路、伺服器、作業系統或儲存），但需要控制上層的應用程式部署與應用代管的環境。

+ <https://zh.wikipedia.org/wiki/%E5%B9%B3%E5%8F%B0%E5%8D%B3%E6%9C%8D%E5%8A%A1>

![005](assets/002/005.back-end.jpeg)

+ 開發者只要專注於程式的部分
+ 其他的硬體、作業系統、開發環境、網路、資料庫，雲端服務提供基礎設施

### 雲端服務 `付費`

![019](assets/010/019.price.png)

+ 費用詳情 : https://www.heroku.com/pricing
+ 免費使用額度 : 550 小時/月
+ 驗證過信用卡 : 贈送 450 小時/月，共1,000 小時/月 
+ 免費限制 : `概念驗證`
  + 30 分鐘內，沒有連線自動進入休眠 (不消耗額度)
  + 每天至少休眠 8 小時
  + 休眠狀態下，收到請求，需等待 20 秒，喚醒系統
  + 儲存空間 : 512MB
+ 正式環境 : 最低每月 7 元方案，不休眠，後續依據使用者數量與網站流量，再決定是否要升級的設備


<br>

### 部署範例 :
<https://flask-deploy-se.herokuapp.com/>


<br>

### 註冊 Heroku 帳號
+ 官網 : <https://www.heroku.com/>
+ 註冊 : <https://signup.heroku.com/>

**官網**

![001](assets/010/001.heroku.png)

**註冊**

![002](assets/010/002.heroku-signup.png)

**認證 E-Mail**

![003](assets/010/003.heroku-email.png)


<br>

### 登入 Heroku 帳號

![004-1](assets/010/004-1.heroku-login.png)

**服務條款**

![004-2](assets/010/004-2.terms-of-service.png)

![004-3](assets/010/004-3.terms-of-service.png)

**初始儀表板**

![005](assets/010/005.heroku-dashboard.png)

**已建立專案**

![006](assets/010/006.heroku-list.png)


<br>

### 創建新應用

![007](assets/010/007.create-app.png)

### 部署方法

![008](assets/010/008.deploy-app.png)

**工具**
+ Git : <https://git-scm.com/>
+ Sourcetree : <https://www.sourcetreeapp.com/>
+ Heroku CLI : <https://devcenter.heroku.com/articles/heroku-cli>

<br>

### 專案內容

**demo.py**
```py
from flask import Flask, request
import os

app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World !'

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
```

**Procfile**

    運行 Flask Web App 的方法，使用 gunicorn (WSGI Server)

+ demo : 檔案模組名稱
+ app : Flask app 的實例變數。

```
web gunicorn demo:app
```

**runtime.txt**
```
python-3.8.5
```
+ 使用的 Python 版本 `python3 -V`

**requirements.txt**
```
gunicorn
Flask==2.0.2
PyMySQL==1.0.2
Flask-SQLAlchemy==2.5.1
```

+ Python 套件清單，Heroku 依據清單配置環境
  + gunicorn : WSGI Server
  + Flask : 網頁框架
  + PyMySQL : MySQL 連線
  + Flask-SQLAlchemy : ORM 框架

### 部署流程

```
Install the Heroku CLI
Download and install the Heroku CLI.

If you haven't already, log in to your Heroku account and follow the prompts to create a new SSH public key.

$ heroku login
Create a new Git repository
Initialize a git repository in a new or existing directory

$ cd my-project/
$ git init
$ heroku git:remote -a gamma-ray-blogger
Deploy your application
Commit your code to the repository and deploy it to Heroku using Git.

$ git add .
$ git commit -am "make it better"
$ git push heroku master
```

heroku login :
```
enoxs@Enoxs-MBP deploy % heroku login
heroku: Press any key to open up the browser to login or q to exit: 
```

![009](assets/010/009.deploy-login.png)

+ 網頁登入

cd my-project/ :

    複製上面四個檔案到新的資料夾，並且用終端機訪問

git init :

    將資料夾初始化為 Git 版本控制專案

+ 目錄內會多一個 .git 的資料夾(隱藏檔案)，代表初始化成功

heroku git:remote -a gamma-ray-blogger :

    將此 Git 專案，配置遠端的存儲庫，也就是 Heroku Git

git add . :

    將全部的內容，加入到版控

git commit -am "make it better" :


```
[master (root-commit) ba6ec25] make it better
 4 files changed, 18 insertions(+)
 create mode 100644 Procfile
 create mode 100644 demo.py
 create mode 100644 requirements.txt
 create mode 100644 runtime.txt
```

+ 提交版本控制到本地端，訊息為 make it better

git push heroku master : 

```
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (6/6), 568 bytes | 568.00 KiB/s, done.
Total 6 (delta 0), reused 0 (delta 0), pack-reused 0
remote: Compressing source files... done.
remote: Building source:
remote: 
remote: -----> Building on the Heroku-20 stack
remote: -----> Determining which buildpack to use for this app
remote: -----> Python app detected
remote: -----> Using Python version specified in runtime.txt
remote:  !     Python has released a security update! Please consider upgrading to python-3.8.12
remote:        Learn More: https://devcenter.heroku.com/articles/python-runtimes
remote: -----> Installing python-3.8.5
remote: -----> Installing pip 21.3.1, setuptools 57.5.0 and wheel 0.37.0
remote: -----> Installing SQLite3
remote: -----> Installing requirements with pip
remote:        Collecting gunicorn
remote:          Downloading gunicorn-20.1.0-py3-none-any.whl (79 kB)
remote:        Collecting Flask==2.0.2
remote:          Downloading Flask-2.0.2-py3-none-any.whl (95 kB)
remote:        Collecting PyMySQL==1.0.2
remote:          Downloading PyMySQL-1.0.2-py3-none-any.whl (43 kB)
remote:        Collecting Flask-SQLAlchemy==2.5.1
remote:          Downloading Flask_SQLAlchemy-2.5.1-py2.py3-none-any.whl (17 kB)
remote:        Collecting itsdangerous>=2.0
remote:          Downloading itsdangerous-2.0.1-py3-none-any.whl (18 kB)
remote:        Collecting Werkzeug>=2.0
remote:          Downloading Werkzeug-2.0.3-py3-none-any.whl (289 kB)
remote:        Collecting click>=7.1.2
remote:          Downloading click-8.0.3-py3-none-any.whl (97 kB)
remote:        Collecting Jinja2>=3.0
remote:          Downloading Jinja2-3.0.3-py3-none-any.whl (133 kB)
remote:        Collecting SQLAlchemy>=0.8.0
remote:          Downloading SQLAlchemy-1.4.31-cp38-cp38-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl (1.6 MB)
remote:        Collecting MarkupSafe>=2.0
remote:          Downloading MarkupSafe-2.0.1-cp38-cp38-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_12_x86_64.manylinux2010_x86_64.whl (30 kB)
remote:        Collecting greenlet!=0.4.17
remote:          Downloading greenlet-1.1.2-cp38-cp38-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (156 kB)
remote:        Installing collected packages: MarkupSafe, Werkzeug, Jinja2, itsdangerous, greenlet, click, SQLAlchemy, Flask, PyMySQL, gunicorn, Flask-SQLAlchemy
remote:        Successfully installed Flask-2.0.2 Flask-SQLAlchemy-2.5.1 Jinja2-3.0.3 MarkupSafe-2.0.1 PyMySQL-1.0.2 SQLAlchemy-1.4.31 Werkzeug-2.0.3 click-8.0.3 greenlet-1.1.2 gunicorn-20.1.0 itsdangerous-2.0.1
remote: -----> Discovering process types
remote:        Procfile declares types -> web
remote: 
remote: -----> Compressing...
remote:        Done: 54.9M
remote: -----> Launching...
remote:        Released v3
remote:        https://gamma-ray-blogger.herokuapp.com/ deployed to Heroku
remote: 
remote: Verifying deploy... done.
To https://git.heroku.com/gamma-ray-blogger.git
 * [new branch]      master -> master
```

+ 推送到 Heroku Git 遠端分支 master，完成後沒有錯誤訊息就代表部署完成
  + `上面 git 操作的部分，也可以使用 Sourcetree 完成`

部署完成 : 

![010-1](assets/010/010-1.deploy-succ.png)

+ demo : https://gamma-ray-blogger.herokuapp.com/

儀表板訊息 :

![010-2](assets/010/010-2.deploy-status.png)

<br>

### 資料庫功能

**添加 MySQL 資料庫**

![011-1](assets/010/011-1.add-database.png)

![011-2](assets/010/011-2.add-database.png)

**Heroku 組件中心，搜尋 ClearDB MySQL**

![011-3](assets/010/011-3.add-database.png)

![011-4](assets/010/011-4.add-database.png)

**安裝 ClearDB MySQL，配置到目標 App**
![011-5](assets/010/011-5.add-database.png)

![011-6](assets/010/011-6.add-database.png)

![011-7](assets/010/011-7.add-database.png)

**配置的過程，若出現以下的錯誤訊息，是因為使用 Heroku 額外的組件需綁定信用卡**

![012-1](assets/010/012-1.pay-bill.png)

右上角的小圖示 > 帳號設定

![012-2](assets/010/012-2.pay-bill.png)

Manage Account > Billing > Add Credit Card

![012-3](assets/010/012-3.pay-bill.png)

**配置完成後，呈現的列表資訊**

![011-8](assets/010/011-8.add-database.png)

![011-9](assets/010/011-9.add-database.png)

<br>

### 資料庫連線資訊

**在列表資訊點擊 ClearDB MySQL 元件**

![013-1](assets/010/013-1.database-info.png)

![013-2](assets/010/013-2.database-info.png)

![013-3](assets/010/013-3.database-info.png)

**指令 : heroku config**

```
heroku config
=== gamma-ray-blogger Config Vars
CLEARDB_DATABASE_URL: mysql://bf78ffc965d109:d1327123@us-cdbr-east-05.cleardb.net/heroku_448e8780cc21c69?reconnect=true
```

**Dbeaver**
+ Host : us-cdbr-east-05.cleardb.net
+ Port : 3306
+ Database : heroku_448e8780cc21c69
+ Username : bf78ffc965d109
+ Password : d1327123

MySQL Driver :
<https://www.mysql.com/products/connector/>

**測試資料**
```sql
-- 應用程式資訊
create table app_info (
    id int auto_increment primary key ,
    name    nvarchar(50),
    version nvarchar(30),
    author  nvarchar(50),
    remark  nvarchar(100)
);

insert into app_info(name,version,author,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','Java Web Application Spring MVC');

select * from app_info;
```

<br>

### 測試資料庫連線

**data.py**
```py
from flask import Flask, request
import os
import pymysql
import pymysql.cursors

app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World !'

@app.route('/query')
def appInfo():
    conn = pymysql.connect(host='us-cdbr-east-05.cleardb.net',
                           user='bf78ffc965d109',
                           password='d1327123',
                           db='heroku_448e8780cc21c69',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    msg = ""
    with conn.cursor() as cursor:
        sql = "select * from app_info;"
        cursor.execute(sql)
        records = cursor.fetchall()
        print("records => ", records)
        for r in records:
            print("r => ", r)
            msg += "{} , {} , {} , {} , {} <br>".format(
                r["id"], r["name"], r["version"], r["author"], r["remark"])
        print("msg => ", msg)
    return msg

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
```

**修改 Procfile**

```
web gunicorn data:app
```

+ demo -> data

**使用 Sourcetree 操作**

![014](assets/010/014.sourcetree.png)

推送訊息 : 
```
git --no-optional-locks -c color.branch=false -c color.diff=false -c color.status=false -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree commit -q -F /var/folders/xb/s22_mchd0lv6mhyj151sln1c0000gn/T/SourceTreeTemp.2CpeOI 


git --no-optional-locks -c color.branch=false -c color.diff=false -c color.status=false -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree push -v --tags --set-upstream heroku refs/heads/master:refs/heads/master 
Pushing to https://git.heroku.com/gamma-ray-blogger.git
POST git-receive-pack (1055 bytes)
remote: Compressing source files... done.        
remote: Building source:        
remote: 
remote: -----> Building on the Heroku-20 stack        
remote: -----> Using buildpack: heroku/python        
remote: -----> Python app detected        
remote: -----> Using Python version specified in runtime.txt        
remote:  !     Python has released a security update! Please consider upgrading to python-3.8.12        
remote:        Learn More: https://devcenter.heroku.com/articles/python-runtimes        
remote: -----> No change in requirements detected, installing from cache        
remote: -----> Using cached install of python-3.8.5        
remote: -----> Installing pip 21.3.1, setuptools 57.5.0 and wheel 0.37.0        
remote: -----> Installing SQLite3        
remote: -----> Installing requirements with pip        
remote: -----> Discovering process types        
remote:        Procfile declares types -> web        
remote: 
remote: -----> Compressing...        
remote:        Done: 54.9M        
remote: -----> Launching...        
remote:        Released v6        
remote:        https://gamma-ray-blogger.herokuapp.com/ deployed to Heroku        
remote: 
remote: Verifying deploy... done.        
To https://git.heroku.com/gamma-ray-blogger.git
   ba6ec25..21a56ee  master -> master
updating local tracking ref 'refs/remotes/heroku/master'
Branch 'master' set up to track remote branch 'master' from 'heroku'.
Completed successfully
```
訪問網站 :
![015](assets/010/015.query.png)

+ demo : <https://gamma-ray-blogger.herokuapp.com/query>

![016](assets/010/016.dbeaver.png)

### 部署 : 第 9 章 綜合實作的專案 [LINK](009.PythonFlask_綜合實作.md)

+ app.py `003.WebImpl.py`
+ main.py `Flask 配置與資料庫資訊`
+ model.py `ORM 物件`
+ Procfile `Heroku 配置`
+ runtime.txt `Heroku 配置`
+ requirements.txt `Heroku 配置`
+ templates/
  + home.html
+ static/
  + js/...
  + img/...
  + css/...
+ db/
  + create.db
  + data.db

**Procfile**

```
web gunicorn app:app
```

+ 應用程式的啟動檔案為 app.py

**main.py**
```diff
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# 取得伺服器目前路徑
UPLOAD_FOLDER = os.getcwd() + os.path.sep + 'static/img'

# 檔案型態
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
app.secret_key = 'FlaskSE'
- app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://DevAuth:Dev127336@127.0.0.1:3306/DevDb"
+ app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://bf78ffc965d109:d1327123@us-cdbr-east-05.cleardb.net:3306/heroku_448e8780cc21c69"
 
# 上傳路徑
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# 限制大小 16MB
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

db = SQLAlchemy(app)
```

**crate.db**
```sql
-- 使用者資訊
create table user_info (
    id int auto_increment primary key ,
    name    nvarchar(50),
    account    nvarchar(30),
    password    nvarchar(50),
    photo  nvarchar(20)
);

-- 部落格文章
create table article (
    id int auto_increment primary key ,
    title    nvarchar(200),
    content    nvarchar(1200),
    author    nvarchar(30),
    date    datetime  
);
```

**data.db**
```sql
-- 使用者資訊
insert into user_info(id , name , account , password , photo) values
(1,'開發者','DevAuth','4a7d1ed414474e4033ac29ccb8653d9b', 'DevAuth.png'),
(2,'測試員','Demo','81dc9bdb52d04dc20036dbd8313ed055', 'Demo.jpg');

-- 部落格文章
insert into article(id , title , content , author , date ) values
(1,'PythonFlask','Text Text Text', 'DevAuth' , '2022-01-20'),
(2,'PythonSQL','CURL : Select Insert Update Delete', 'DevAuth' , '2022-01-21' ),
(3,'PythonOCR','Image to Text', 'Demo' , '2022-01-22' );
```

**使用 Sourcetree 操作**

![017](assets/010/017.sorucetruee-sample.png)

推送訊息 : 
```
git --no-optional-locks -c color.branch=false -c color.diff=false -c color.status=false -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree commit -q -F /var/folders/xb/s22_mchd0lv6mhyj151sln1c0000gn/T/SourceTreeTemp.C5uzQ0 


git --no-optional-locks -c color.branch=false -c color.diff=false -c color.status=false -c diff.mnemonicprefix=false -c core.quotepath=false -c credential.helper=sourcetree push -v --tags --set-upstream heroku refs/heads/master:refs/heads/master 
Pushing to https://git.heroku.com/gamma-ray-blogger.git
POST git-receive-pack (226917 bytes)
remote: Compressing source files... done.        
remote: Building source:        
remote: 
remote: -----> Building on the Heroku-20 stack        
remote: -----> Using buildpack: heroku/python        
remote: -----> Python app detected        
remote: -----> Using Python version specified in runtime.txt        
remote:  !     Python has released a security update! Please consider upgrading to python-3.8.12        
remote:        Learn More: https://devcenter.heroku.com/articles/python-runtimes        
remote: -----> No change in requirements detected, installing from cache        
remote: -----> Using cached install of python-3.8.5        
remote: -----> Installing pip 21.3.1, setuptools 57.5.0 and wheel 0.37.0        
remote: -----> Installing SQLite3        
remote: -----> Installing requirements with pip        
remote: -----> Discovering process types        
remote:        Procfile declares types -> web        
remote: 
remote: -----> Compressing...        
remote:        Done: 55.2M        
remote: -----> Launching...        
remote:        Released v7        
remote:        https://gamma-ray-blogger.herokuapp.com/ deployed to Heroku        
remote: 
remote: Verifying deploy... done.        
To https://git.heroku.com/gamma-ray-blogger.git
   21a56ee..9541466  master -> master
updating local tracking ref 'refs/remotes/heroku/master'
Branch 'master' set up to track remote branch 'master' from 'heroku'.
Completed successfully
```
訪問網站 :

![018-1](assets/010/018-1.home.png)

+ demo : <https://gamma-ray-blogger.herokuapp.com/>

登入功能生效 `(資料庫)`

![018-2](assets/010/018-2.login.png)


雲端部署 PythonAnywhere 並且連線 MySQL
------
`補充資料`

+ <https://www.pythonanywhere.com/>
+ <https://itcosmos.co/create-website-on-pythonanywhere/>

### 雲端服務，免費限制
+ 只能建立一個 App (應用程式)
+ 網外存取 Internet 有限制
+ CPU 與儲存有限制 (一天 100 秒 CPU 時間, 512MB 儲存)
+ 不提供 Jupyter (但有 IPython)
+ 只能有兩個 Console (Bash 與 Python)

### GitLab Project
`Python Web`

+ <https://gitlab.com/GammaRayStudio/HiSKIO/Sample/PythonWeb>

### 部署 Python Web 專案
1. Dashboard > $ Bash
2. cmd : `git clone https://gitlab.com/GammaRayStudio/HiSKIO/Sample/PythonWeb.git`
3. Dashboard > File > PythonWeb > demo.py
4. Dashboard > Web apps > Add a new web app 
    + Python Web framework : `Flask`
    + Python Version : `Python 3.8`
    + Project Path : `/home/Enoxs/PythonWeb/demo.py`
5. Update : demo.py
6. Browse : Enoxs.pythonanywhere.com

### 配置 MySQL 資料庫
`Databases`

+ Database host address
+ Username
+ Your databases
+ MySQL Password

建立虛擬環境 :
```
mkvirtualenv --python=/usr/bin/python3.8 python-web-virtualenv
```

安裝套件 :
```
pip install Flask
pip install PyMySQL
pip install Flask_SQLAlchemy
```

創建新的 Web App:
1. Dashboard > Web apps > Add a new web app 
    + Python Web framework : `Manual configuration`
    + Python Version : `Python 3.8`
2. Web > Code > Source code : `/home/Enoxs/PythonWeb`
3. Web > Code > WSGI configuration file: `/var/www/enoxs_pythonanywhere_com_wsgi.py`
4. Web > Code > Virtualenv: `python-web-virtualenv`
5. Web > Reload Enoxs.pythonanywhere.com

WSGI configuration file:
```py
# +++++++++++ FLASK +++++++++++
import sys
path = '/home/Enoxs/PythonWeb'
if path not in sys.path:
    sys.path.append(path)

from demo import app as application  # noqa
```

Virtual Environment
```
進入虛擬環境:

  workon python-web-virtualenv

退出虛擬環境: 

  deactivate
```

**訪問**

<https://enoxs.pythonanywhere.com/query>

![020](assets/010/020.python-anywhere-mysql.png)

<https://enoxs.pythonanywhere.com/home>

![021](assets/010/021.python-anywhere-system.png)


其他的雲端服務
------
`補充資料`

+ GCP VS HEROKU 小資使用心得 : <https://blog.typeart.cc/heroku-vs-gcp/>



參考資料
------
### 用 Heroku 部署網站
<https://djangogirlstaipei.herokuapp.com/tutorials/deploy-to-heroku/?os=windows>

### Heroku 是什麼 ?! 與 AWS 的差異 ?
<https://matters.news/@johnnymnotes/heroku-%E6%98%AF%E4%BB%80%E9%BA%BC-%E8%88%87-aws-%E7%9A%84%E5%B7%AE%E7%95%B0-bafyreicjzmmidb5a7far4pi3kgnz6tm4sbzna4cknlejqjuqgdxv3yjj6e>

### Heroku vs Python Where
<https://stackshare.io/stackups/heroku-vs-pythonanywhere>

### Python Where - Using MySQL
<https://help.pythonanywhere.com/pages/UsingMySQL/>

### 免費的線上 Python 執行環境 PythonAnywhere
<http://yhhuang1966.blogspot.com/2019/03/python-pythonanywhere.html>

### Django項目部署到pythonanywhere及需要注意的問題
<https://www.twblogs.net/a/5d401620bd9eee51fbf96ad7>

### Getting started with App Engine (Python 3)
<https://codelabs.developers.google.com/codelabs/cloud-app-engine-python3/index.html#0>

### Cloud SQL 定價
<https://cloud.google.com/sql/pricing?hl=zh-TW>

### 使用 Google VM 的5大常見錯誤 – 有一種叫忘記關機！
<https://blog.cloud-ace.tw/infrastructure/vm/5-big-mistakes-of-using-google-vm/>

### 【Flask 教學】實作 GCP 部署 Flask + Nginx + uWSGI
<https://www.maxlist.xyz/2020/06/17/flask-nginx-uwsgi-on-gcp/>

### Google 免費主機 GCP 設定教學，不花 1 毛錢輕鬆架設 WordPress 網站
<https://weblai.co/wordpress-gcp-tutorial/#GCP_zhe_me_hao_wei_shen_me_mei_you_da_jia_dou_lai_yong>


